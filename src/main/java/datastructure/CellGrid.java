package datastructure;

import cellular.CellState;

public class CellGrid implements IGrid {

    public int cols;
    public int rows;
    public CellState[][] cellStates;

    public CellGrid(int rows, int columns, CellState initialState) {
        this.rows = rows;
        this.cols = columns;
        cellStates = new CellState[rows][columns];
        for (int col = 0; col < cols; col++) {
            for (int row = 0; row < rows; row++) {
                cellStates[row][col] = initialState;

            }
        }
	}

    @Override
    public int numRows() {
        return rows;
    }

    @Override
    public int numColumns() {
        return cols;
    }

    @Override
    public void set(int row, int column, CellState element) {
        if (row < 0 || row > numRows()) {
            throw new IndexOutOfBoundsException("row cannot be a negative number");
        }
        if (column < 0 || column > numColumns()){
            throw new IndexOutOfBoundsException("column cannot be a negitve number");
        }
        cellStates[row][column] = element;
        
        }

    @Override
    public CellState get(int row, int column) {
        if (row < 0 && row > numRows()) {
            throw new IndexOutOfBoundsException("row cannot be a negative number");
        }
        if (column < 0 && column > numColumns()){
            throw new IndexOutOfBoundsException("column cannot be a negitve number");
        }
        return cellStates[row][column];
    }

    @Override
    public IGrid copy() {
        CellGrid copyGrid = new CellGrid(this.rows, this.cols, null);
        for (int col = 0; col < cols; col++) {
            for (int row = 0; row < rows; row++) {
                copyGrid.set(row, col, this.get(row, col));
            }
        }
        return copyGrid;
    } 
}
